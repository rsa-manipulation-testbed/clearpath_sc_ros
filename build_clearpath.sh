#!/bin/bash


CLEARPATH_SDK=Clearpath_Linux_Software_2021-04-12.tar.gz
CLEARPATH_URL=https://s3.us-west-1.wasabisys.com/marburg/$CLEARPATH_SDK
TMP_DIR=tmp_clearpath

#/usr/local/stow/clearpath_sdk
INSTALL_PREFIX=/usr/local


## Check if already installed
if [[ -f /usr/local/lib/libsFoundation20.so ]]; then
    echo "SDK appears to already be installed."
    exit 0
fi


if [[ ! -d $TMP_DIR ]]; then
    mkdir $TMP_DIR
fi
cd $TMP_DIR

if [[ ! -f $CLEARPATH_SDK ]]; then
    wget $CLEARPATH_URL
fi

tar -xzvf $CLEARPATH_SDK
tar -xvf sFoundation.tar
cd sFoundation

make -j

sudo mkdir -p $INSTALL_PREFIX/lib
sudo cp {MNuserDriver20.xml,libsFoundation20.so} $INSTALL_PREFIX/lib/
cd $INSTALL_PREFIX/lib && sudo ln -s libsFoundation20.so libsFoundation20.so.1

sudo mkdir -p $INSTALL_PREFIX/include/Clearpath/
cd .. && sudo cp -r inc/* $INSTALL_PREFIX/include/Clearpath/
