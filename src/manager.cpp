#include "clearpath_sc_ros/manager.h"

using namespace sFnd;

namespace clearpath_sc {

Manager::Manager() : _mgr(SysManager::Instance()) {}

Manager::~Manager() {
  // Axes try to shut themselves down during destruction
  // So manager must be running at in destructor
  _axes.clear();

  // If not done previously
  _mgr->PortsClose();
}

void Manager::initialize() {
  std::vector<std::string> portNames;
  SysManager::FindComHubPorts(portNames);

  // Editing of the list of portnames (e.g. block-listing particular
  // ports) could happen here.

  _numPorts = portNames.size();
  for (size_t portNum = 0;
       portNum < portNames.size() && portNum < NET_CONTROLLER_MAX; portNum++) {
    _mgr->ComHubPort(portNum, portNames[portNum].c_str());
  }

  _mgr->PortsOpen(_numPorts);

  for (size_t portNum = 0; portNum < _numPorts; ++portNum) {
    IPort &port = _mgr->Ports(portNum);

    for (size_t nodeNum = 0; nodeNum < port.NodeCount(); nodeNum++) {
      INode &node = port.Nodes(nodeNum);

      // Make sure we are talking to a ClearPath SC (advanced or basic model
      // will work)
      if (node.Info.NodeType() != IInfo::CLEARPATH_SC_ADV &&
          node.Info.NodeType() != IInfo::CLEARPATH_SC) {
        //("---> ERROR: Uh-oh! Node %d is not a ClearPath-SC Motor\n", iNode);
        continue;
      }

      // Create an axis for this node
      _axes.push_back(std::make_shared<Axis>(node));

      // // Make sure we have full access
      // if (!node.Setup.AccessLevelIsFull())
      // {
      //     printf("---> ERROR: Oh snap! Access level is not good for node
      //     %u\n", iNode); accessLvlsGood = false;
      // }
    }
  }
}

// try
// {

// 	for (portCount = 0; portCount < comHubPorts.size() && portCount <
// NET_CONTROLLER_MAX; portCount++) {

// 		myMgr->ComHubPort(portCount, comHubPorts[portCount].c_str());
// //define the first SC Hub port (port 0) to be associated
// 										//
// with COM portnum (as seen in device manager)
// 	}

// 	if (portCount > 0) {
// 		//printf("\n I will now open port \t%i \n \n", portnum);
// 		myMgr->PortsOpen(portCount);				//Open
// the port

// 		for (size_t i = 0; i < portCount; i++) {
// 			IPort &myPort = myMgr->Ports(i);

// 			printf(" Port[%d]: state=%d, nodes=%d\n",
// 				myPort.NetNumber(), myPort.OpenState(),
// myPort.NodeCount());
// 		}
// 		// Close down the ports
// 		myMgr->PortsClose();
// 	}
// 	else {
// 		printf("Unable to locate SC hub port\n");

// 		msgUser("Press any key to continue."); //pause so the user can
// see the error message; waits for user to press a key

// 		return -1;  //This terminates the main program
// 	}
// }
// catch(mnErr& theErr)	//This catch statement will intercept any error from the
// Class library
// {
// 	printf("Port Failed to open, Check to ensure correct Port number and
// that ClearView is not using the Port\n");
// 	//This statement will print the address of the error, the error code
// (defined by the mnErr class),
// 	//as well as the corresponding error message.
// 	printf("Caught error: addr=%d, err=0x%08x\nmsg=%s\n", theErr.TheAddr,
// theErr.ErrorCode, theErr.ErrorMsg);

// 	msgUser("Press any key to continue."); //pause so the user can see the
// error message; waits for user to press a key

// 	return -1;  //This terminates the main program
// }

}  // namespace clearpath_sc
