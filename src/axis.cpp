//
// Based on the sample "Axis.cpp" from the Clearpath SDK exampls
//
///*******************************************************************************
//
// NAME
//		Axis
//
// DESCRIPTION:
//		This class holds the state machine and handles communication
//		directly with the ClearPath-SC node
//																			   *
//******************************************************************************

#include "clearpath_sc_ros/axis.h"

#include <stdio.h>

#include <iostream>

namespace clearpath_sc {

Axis::Axis(INode &node)
    : _node(node), m_moveCount(0), m_positionWrapCount(0), m_quitting(false) {
  // OPTIONAL - Save the config file before starting
  // This would typically be performed right after tuning each axis.
  // Chosing a file name that contains the address and serial number
  // can ensure that the system does not get miswired.
  // Using the UserID as a filename key can allow swapping motors with
  // minimal setup required. This requires that no invalid characters
  // are present in the UserID.
  snprintf(m_configFile, sizeof(m_configFile), "/tmp/config-%02d-%d.mtr",
           _node.Info.Ex.Addr(), _node.Info.SerialNumber.Value());

  if (_node.Setup.AccessLevelIsFull()) _node.Setup.ConfigSave(m_configFile);
};
//																			   *
//******************************************************************************

Axis::~Axis() {
  // Print out the move statistics one last time
  // PrintStats();

  // If we don't have full access, there's nothing to do here
  if (!_node.Setup.AccessLevelIsFull()) {
    return;
  }

  // Disable the node and wait for it to disable
  _node.EnableReq(false);

  // Poll the status register to confirm the node's disable
  time_t timeout;
  timeout = time(NULL) + 3;
  _node.Status.RT.Refresh();
  while (_node.Status.RT.Value().cpm.Enabled) {
    if (time(NULL) > timeout) {
      printf("Error: Timed out waiting for disable\n");
      return;
    }
    _node.Status.RT.Refresh();
  };

  // Restore the original config file
  _node.Setup.ConfigLoad(m_configFile);
}
//																			   *
//******************************************************************************

//******************************************************************************
//	NAME
//* 		Axis::Enable
//
//	DESCRIPTION:
//		Clear alerts and get the node ready to roll
//
void Axis::enable() {
  // Clear alerts and node stops
  _node.Status.AlertsClear();
  _node.Motion.NodeStop(STOP_TYPE_ABRUPT);
  _node.Motion.NodeStop(STOP_TYPE_CLR_ALL);

  // Enable the node
  _node.EnableReq(true);

  // If the node is not currently ready, wait for it to get there
  time_t timeout;
  timeout = time(NULL) + 3;
  // Basic mode - Poll until disabled
  while (!_node.Status.IsReady()) {
    if (time(NULL) > timeout) {
      printf("Error: Timed out waiting for enable\n");
      return;
    }
  };

  // If node is set up to home, start homing
  // if (_node.Motion.Homing.HomingValid()) {
  //   _node.Motion.Homing.Initiate();
  //   printf("Homing...");
  //   time_t timeout;
  //   timeout = time(NULL) + 10000;
  //   // Basic mode - Poll until disabled
  //   while (!_node.Motion.Homing.WasHomed()) {
  //     if (time(NULL) > timeout) {
  //       std::cerr << "Error: Timed out waiting for homing" << std:err;;
  //       return;
  //     }
  //   };
  //   printf("Completed\n");
  // }
}

void Axis::disable() {
  // Disable the node and wait for it to disable
  _node.EnableReq(false);

  // Poll the status register to confirm the node's disable
  time_t timeout;
  timeout = time(NULL) + 3;
  _node.Status.RT.Refresh();
  while (_node.Status.RT.Value().cpm.Enabled) {
    if (time(NULL) > timeout) {
      std::cerr << "Error: Timed out waiting for disable" << std::endl;
      return;
    }
    _node.Status.RT.Refresh();
  };
}

//																			   *
//******************************************************************************

//******************************************************************************
//	NAME
//* 		Axis::InitMotionParams
//
//	DESCRIPTION:
//		Initialize accLim, velLim, etc
//
void Axis::InitMotionParams() {
  // Set the user units to RPM and RPM/s
  _node.VelUnit(INode::RPM);
  _node.AccUnit(INode::RPM_PER_SEC);

  // Set the motion's limits
  _node.Motion.VelLimit = VEL_LIM_RPM;
  _node.Motion.AccLimit = ACC_LIM_RPM_PER_SEC;
  _node.Motion.JrkLimit = RAS_CODE;
  _node.Limits.PosnTrackingLimit.Value(TRACKING_LIM);
  _node.Limits.TrqGlobal.Value(TRQ_PCT);

  // Set the dwell used by the motor when applicable
  _node.Motion.DwellMs = 5;
}
//																			   *
//******************************************************************************

//******************************************************************************
//	NAME
//* 		Axis::ChangeMotionParams
//
//	DESCRIPTION:
//		Modify velLimit, AccLimit, etc.
//
void Axis::ChangeMotionParams(double newVelLimit, double newAccelLimit,
                              uint32_t newJrkLimit, uint32_t newTrackingLimit,
                              int32_t newTrqGlobal) {
  // Set the motion's limits
  _node.Motion.VelLimit = newVelLimit;
  _node.Motion.AccLimit = newAccelLimit;
  _node.Motion.JrkLimit = newJrkLimit;
  _node.Limits.PosnTrackingLimit.Value(newTrackingLimit);
  _node.Limits.TrqGlobal.Value(newTrqGlobal);
}
//																			   *
//******************************************************************************

//******************************************************************************
//	NAME
//* 		Axis::PosnMove
//
//	DESCRIPTION:
//		Issue a position move
//
void Axis::positionMove(int32_t targetPosn, bool isAbsolute, bool addDwell) {
  // Save the move's calculated time for timeout handling
  m_moveTimeoutMs =
      (int32_t)_node.Motion.MovePosnDurationMsec(targetPosn, isAbsolute);
  m_moveTimeoutMs += 20;

  // Should be a delay here...

  // Check for number-space wrapping. With successive moves, the position may
  // pass the 32-bit number cap (+ or -). The check below looks for that and
  // updates our counter to be used with GetPosition
  if (!isAbsolute) {
    if (position(false) + targetPosn > INT32_MAX) {
      m_positionWrapCount++;
    } else if (position(false) + targetPosn < INT32_MIN) {
      m_positionWrapCount--;
    }
  }

  // Issue the move to the node
  _node.Motion.MovePosnStart(targetPosn, isAbsolute, addDwell);
}
//																			   *
//******************************************************************************

//******************************************************************************
//	NAME
//* 		Axis::VelMove
//
//	DESCRIPTION:
//		Issue a velocity move
//
void Axis::velocityMove(double targetVel) {
  // Issue the move to the node
  _node.Motion.MoveVelStart(targetVel);

  // Move at the set velocity for half a second
  // m_sysMgr.Delay(500);

  // Issue a node stop, forcing any movement (like a
  // velocity move) to stop. There are several
  // different types of node stops
  _node.Motion.NodeStop(STOP_TYPE_ABRUPT);
  // _node.Motion.NodeStop(STOP_TYPE_RAMP);

  // Configure the timeout for the node stop to settle
  m_moveTimeoutMs = 100;
}
//																			   *
//******************************************************************************

//******************************************************************************
//	NAME
//* 		Axis::WaitForMove
//
//	DESCRIPTION:
//		Wait for attention that move has completed
//
bool Axis::WaitForMove(::int32_t timeoutMs, bool secondCall) {
  // Wait for the proper calculated move time
  time_t timeout;
  timeout = time(NULL) + timeoutMs;

  _node.Status.RT.Refresh();
  // Basic mode - Poll until move done
  while (!_node.Status.RT.Value().cpm.MoveDone) {
    _node.Status.RT.Refresh();
    if (time(NULL) > timeout) {
      printf("Error: Timed out during move\n");
      return false;
    }

    // Catch specific errors that can occur during a move
    if (_node.Status.RT.Value().cpm.Disabled) {
      printf("  ERROR: [%d] disabled during move\n", _node.Info.Ex.Addr());
      return false;
    }
    if (_node.Status.RT.Value().cpm.NotReady) {
      printf("  ERROR: [%d] went NotReady during move\n", _node.Info.Ex.Addr());
      return false;
    }
  }

  return _node.Status.RT.Value().cpm.MoveDone;
};
//																			   *
//******************************************************************************

//******************************************************************************
//	NAME
//* 		Axis::AxisMain
//
//	DESCRIPTION:
//		The main work of the axis class - initialize the node and run
//		the state machine
//
void Axis::AxisMain() {
  bool moveDone;

  try {
    // Get the node ready to go
    // Enable();
    // Initialize the motion values
    InitMotionParams();
  } catch (mnErr &err) {
    fprintf(stderr, "Setup error: %s\n", err.ErrorMsg);
  } catch (...) {
    fprintf(stderr, "Setup failed generically.\n");
  }

  if (!m_quitting) {
    // If your node has appropriate hardware (hardstop/home switch)
    // and is configured in ClearView, uncomment the line below to
    // initiate homing
    // theNode.Motion.Homing.Initiate();

    // Set the hardware brake to allow motion
    _node.Port.BrakeControl.BrakeSetting(0, BRAKE_ALLOW_MOTION);

    // Zero out the commanded position
    _node.Motion.PosnCommanded.Refresh();
    double posn = _node.Motion.PosnCommanded.Value();
    _node.Motion.AddToPosition(-posn);
    _node.Motion.PosnCommanded.Refresh();
    // PrintStats(true);
  }

  m_state = STATE_IDLE;

  // Start the machine cycling
  while (!m_quitting) {
    try {
      // Save last state for debugging purposes
      m_lastState = m_state;

      // Check for alerts
      CheckForAlerts();

      switch (m_state) {
        case STATE_IDLE:
          // Update idle state
          // Quitting?
          if (m_quitting) continue;
          m_state = STATE_SEND_MOVE;
          break;
        case STATE_SEND_MOVE:
          // Initiate the motion
          if (m_moveCount % 2 == 0) {
            positionMove(5000);
          } else {
            // Set new motion parameters before the next move
            ChangeMotionParams(1500, 7500, 4,
                               _node.Info.PositioningResolution.Value() / 4,
                               95);

            velocityMove(50);
          }

          // Reset our motion parameters.  The parameters for the
          // next move could be entered instead to get a head start
          InitMotionParams();

          m_state = STATE_WAIT_FOR_MOVE_DONE;
          break;
        case STATE_WAIT_FOR_MOVE_DONE:
          // Check if the move is done yet
          moveDone = WaitForMove(m_moveTimeoutMs);
          if (!moveDone) {
            printf("ERROR: [%2d] timed out waiting for move done\n",
                   _node.Info.Ex.Addr());
            return;
          } else {
            m_moveCount++;
            m_state = STATE_IDLE;
          }
          if (m_moveCount == 4) {
            // Stop once we've properly demonstrated movement
            Quit();
          }
          // PrintStats();
          if (m_quitting) continue;
          break;
        default:
          fprintf(stderr, "Illegal state");
          return;
      }
    }  // Try block
    catch (mnErr &err) {
      fprintf(stderr, "Link error: %s\n", err.ErrorMsg);
      ResetToIdle();
      continue;
    } catch (...) {
      fprintf(stderr, "Node failed generically.\n");
      ResetToIdle();
    }
  }

  m_state = STATE_EXITED;
  ResetToIdle();
  printf("[%s] Quitting node...\n", _node.Info.UserID.Value());
  return;
}
//																			   *
//******************************************************************************

//******************************************************************************
//	NAME
//* 		Axis::CheckForAlerts
//
//	DESCRIPTION:
//		Checks the status register if there are alerts present.
//		If there are, print some information about them and quit.
//
void Axis::CheckForAlerts() {
  // Refresh our relevant registers
  _node.Status.Alerts.Refresh();
  _node.Status.RT.Refresh();

  // Check for alerts
  if (_node.Status.RT.Value().cpm.AlertPresent) {
    char alertList[256];
    _node.Status.Alerts.Value().StateStr(alertList, 256);
    printf("Node %d has alerts! Alert: %s", _node.Info.Ex.Addr(), alertList);
    m_quitting = true;
  }
}
//																			   *
//******************************************************************************

//******************************************************************************
//	NAME
//* 		Axis::SetBrakeControl
//
//	DESCRIPTION:
//		Setup the hardware brake control on the node's port. Set's the
// indicated 		brake's mode to the passed-in value
//
void Axis::SetBrakeControl(size_t brakeNum, BrakeControls brakeMode) {
  _node.Port.BrakeControl.BrakeSetting(brakeNum, brakeMode);
}
//																			   *
//******************************************************************************

//******************************************************************************
//	NAME
//* 		Axis::GetPosition
//
//	DESCRIPTION:
//		Return the node's commanded position scaled properly to wraps of
// the 		number space.
//
// int64_t Axis::position(bool includeWraps) {
//   // Create a variable to return and refresh the position
//   long long scaledPosn;
//   _node.Motion.PosnCommanded.Refresh();

//   // If there have been no number wraps, just return the position
//   scaledPosn = int64_t(_node.Motion.PosnCommanded.Value());

//   if (includeWraps) {
//     scaledPosn += int64_t(m_positionWrapCount) << 32;
//   }

//   return scaledPosn;
// }

//******************************************************************************
//	NAME
//* 		Axis::position
//
//	DESCRIPTION:
//		Return the node's current position scaled properly to wraps of
// the 		number space.
//
int64_t Axis::position(bool includeWraps) {
  // Create a variable to return and refresh the position
  long long scaledPosn;
  _node.Motion.PosnMeasured.Refresh();

  // If there have been no number wraps, just return the position
  scaledPosn = int64_t(_node.Motion.PosnMeasured.Value());

  if (includeWraps) {
    scaledPosn += int64_t(m_positionWrapCount) << 32;
  }

  return scaledPosn;
}

int64_t Axis::velocity() {
  // Create a variable to return and refresh the position
  _node.Motion.VelMeasured.Refresh();
  return int64_t(_node.Motion.VelMeasured.Value());
}

int64_t Axis::torque() {
  // Create a variable to return and refresh the position
  _node.Motion.TrqMeasured.Refresh();
  return int64_t(_node.Motion.TrqMeasured.Value());
}

int64_t Axis::positionCmd(bool includeWraps) {
  // Create a variable to return and refresh the position
  long long scaledPosn;
  _node.Motion.PosnCommanded.Refresh();

  // If there have been no number wraps, just return the position
  scaledPosn = int64_t(_node.Motion.PosnCommanded.Value());

  if (includeWraps) {
    scaledPosn += int64_t(m_positionWrapCount) << 32;
  }

  return scaledPosn;
}

int64_t Axis::velocityCmd() {
  // Create a variable to return and refresh the position
  _node.Motion.VelCommanded.Refresh();
  return int64_t(_node.Motion.VelCommanded.Value());
}

int64_t Axis::torqueCmd() {
  // Create a variable to return and refresh the position
  _node.Motion.TrqCommanded.Refresh();
  return int64_t(_node.Motion.TrqCommanded.Value());
}

}  // namespace clearpath_sc
