//
// Base on the "Axis.h" from the Clearpath SDK examples
//
/****************************************************************************
 * NAME:
 *		Axis
 *
 * DESCRIPTION:
 *		This class provides an interface to the ClearPath SC node
 *		in our example program
 *
 ****************************************************************************/

#pragma once

#include <time.h>

#include "Clearpath/inc-pub/pubSysCls.h"

namespace clearpath_sc {

using namespace sFnd;
using namespace std;

// class AxisInfo {
// public:
//   AxisInfo(sFnd::IInfo &info) : _info(info) { ; }

//   const std::string &firmwareVersion() const { return _info.FirmwareVersion;
//   } const std::string &modelName() const { return _info.Model; } unsigned int
//   serialNumber() const { return _info.SerialNumber; }

//   sFnd::IInfo &_info;
// };

#define RAS_CODE 3
#define TRQ_PCT 10
#define ACC_LIM_RPM_PER_SEC 800
#define VEL_LIM_RPM 200
#define TRACKING_LIM (_node.Info.PositioningResolution.Value() / 4)
/* !end!
**
****************************************************************************/

//******************************************************************************
// NAME
// * 	Axis class
//
// DESCRIPTION
//	This is the interface to the ClearPath SC.
//
class Axis {
 public:
  Axis(INode &node);
  ~Axis();

  // Retrieve an info struct
  sFnd::IInfo &getInfo() { return _node.Info; }

  // The state machine
  void AxisMain();

  // Enable the node and get it ready to go
  void enable();

  void disable();

  // Return motor state
  int64_t position(bool includeWraps = true);
  int64_t velocity();
  int64_t torque();

  // Return commanded position / velocity / torque
  int64_t positionCmd(bool includeWraps = true);
  int64_t velocityCmd();
  int64_t torqueCmd();

  // Initiate a move
  void positionMove(int32_t targetPosn, bool isAbsolute = false,
                    bool addDwell = false);
  void velocityMove(double targetVel);

  // Reset sequencer to "idle" state
  void ResetToIdle() {
    if (m_state != STATE_IDLE) m_state = m_lastState = STATE_IDLE;
  };

  // Time to quit
  void Quit() { m_quitting = true; }

 private:
  INode &_node;  // The ClearPath-SC for this axis

// Filename for the stored config file
#define MAX_CONFIG_PATH_LENGTH 256
#define MAX_CONFIG_FILENAME_LENGTH 384
  char m_configFile[MAX_CONFIG_FILENAME_LENGTH];

  // State machine information
  enum StateEnum {
    STATE_IDLE,
    STATE_SEND_MOVE,
    STATE_WAIT_FOR_MOVE_DONE,
    STATE_EXITED
  };
  StateEnum m_state;
  StateEnum m_lastState;

  // Move information
  uint32_t m_moveCount;
  int32_t m_positionWrapCount;
  int32_t m_moveTimeoutMs;

  bool m_quitting;  // Axis quitting

  // Initialize accLim, velLim, etc
  void InitMotionParams();

  // Edit the current motion parameters
  void ChangeMotionParams(double newVelLimit, double newAccelLimit,
                          uint32_t newJrkLimit, uint32_t newTrackingLimit,
                          int32_t newTrqGlobal);

  // Wait for attention that move has completed
  bool WaitForMove(::int32_t timeoutMs, bool secondCall = false);

  // Alert handlers
  void CheckForAlerts();

  // HW Brake Setup
  void SetBrakeControl(size_t brakeNum, BrakeControls brakeMode);
};
//																			   *
//******************************************************************************

}  // namespace clearpath_sc
