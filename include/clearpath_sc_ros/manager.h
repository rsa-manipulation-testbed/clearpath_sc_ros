#pragma once

#include <memory>
#include <string>
#include <vector>

#include "Clearpath/inc-pub/pubSysCls.h"
#include "clearpath_sc_ros/axis.h"

namespace clearpath_sc {

class Manager {
 public:
  explicit Manager();
  Manager(const Manager &) = delete;

  ~Manager();

  // Open all ports and build a list of all nodes
  void initialize();
  void close();

  size_t numPorts() const { return _numPorts; }

  const std::vector<std::shared_ptr<Axis>> &axes() const { return _axes; }

 private:
  // Yes, a bare pointer, lifecycle is managed in the Clearpath library itself?
  sFnd::SysManager *_mgr;

  size_t _numPorts;
  std::vector<std::shared_ptr<Axis>> _axes;
};

}  // namespace clearpath_sc
