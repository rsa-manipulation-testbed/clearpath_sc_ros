# Clearpath_SC_ROS

# Note:  this package has been EOL'ed and merged into [testbed_hw](https://gitlab.com/rsa-manipulation-testbed/testbed_hw)

Package for evaluating / testing / demoing the Clearpath "Software Controller" (SC) actuators in Linux / ROS.

# Prerequisites

The Clearpath "Linux Software" must be installed.  The script `build_clearpath.sh` in this repo will automatically download and build the software, and install it in `/usr/local`.  This library must be installed before building this repo.



# License

This repo is released under the [BSD 3-Clause License](LICENSE).
