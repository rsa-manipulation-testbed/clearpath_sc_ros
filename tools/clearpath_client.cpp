// Copyright 2021 University of Washington
//
// Released under the BSD license, see LICENSE in this repo
//
// A simple command line client for running a TMotor in position mode

#include <boost/program_options.hpp>
#include <chrono>
#include <cmath>
#include <csignal>
#include <functional>
#include <iostream>
namespace po = boost::program_options;

#include "clearpath_sc_ros/manager.h"
#include "kb_nonblocking.h"

using namespace clearpath_sc;
using namespace sFnd;

// int canId = 1;

namespace {
std::function<void(int)> signal_function;
void signal_handler(int signal) { signal_function(signal); }
}  // namespace

class ClearpathClient {
 public:
  ClearpathClient() : _done(false) {
    // Signal can't take a lambda directly,
    // but the global "signal_function" can
    signal(SIGINT, signal_handler);
    signal_function = [&](int signum) {
      if (signum == SIGINT) {
        _done = true;
        return;
      }

      std::cout << "Exiting with signal " << signum << std::endl;
      exit(signum);
    };
  }

  ~ClearpathClient() { nonblock(false); }

  //   TMotorClient(CanInterface canif, int canId, TMotorConstants constants)
  //       : _canIf(canif), _canId(canId), _cmd(constants), _initialized(false),
  //         _done(false)

  //   void read_reply(CanInterface &can) {
  //     struct can_frame frame;

  //     if (can.receive(frame)) {
  //       auto state = TMotor::unpackFrame(frame, _cmd.constants);

  //       if (state.id == _canId) {
  //         std::cout << "----" << std::endl;
  //         state.dump();
  //         std::cout << "----" << std::endl;

  //         if (!_initialized) {
  //           std::cerr << "Initializing driver with position: " << state.pos
  //                     << std::endl;

  //           _cmd.setPos(state.pos).setKd(kd).setKp(kp);
  //           _initialized = true;
  //         }
  //       } else {
  //         std::cerr << "Got bad id " << state.id << std::endl;
  //       }
  //     }
  //   }

  void dumpNodes() {
    try {
      _mgr.initialize();
      std::cout << "Found " << _mgr.axes().size() << " nodes on "
                << _mgr.numPorts() << " ports" << std::endl;

      std::shared_ptr<Axis> theAxis;
      for (auto &axis : _mgr.axes()) {
        IInfo &info(axis->getInfo());

        std::cout << "  " << info.Model.Value() << " : s/n "
                  << info.SerialNumber.Value() << " : f/w "
                  << info.FirmwareVersion.Value() << std::endl;
      }
    } catch (mnErr &theErr) {
      printf("Caught error: addr=%d, err=0x%08x\nmsg=%s\n", theErr.TheAddr,
             theErr.ErrorCode, theErr.ErrorMsg);
    }
  }

  void run(int sn) {
    try {
      _mgr.initialize();
      std::cout << "Found " << _mgr.axes().size() << " nodes on "
                << _mgr.numPorts() << " ports" << std::endl;

      std::shared_ptr<Axis> theAxis;
      for (auto &axis : _mgr.axes()) {
        IInfo &info(axis->getInfo());

        std::cout << "      " << info.Model.Value() << " : s/n "
                  << info.SerialNumber.Value() << " : f/w "
                  << info.FirmwareVersion.Value() << std::endl;

        if (int(info.SerialNumber.Value()) == sn) {
          std::cout << "   Found motor s/n " << sn << std::endl;
          theAxis = axis;
        }
      }

      if (!theAxis) {
        std::cerr << "Unable to find a motor with serial number " << sn
                  << std::endl;
        return;
      }

      {
        IInfo &info(theAxis->getInfo());
        std::cout << "Using " << info.Model.Value() << " : s/n "
                  << info.SerialNumber.Value() << " : f/w "
                  << info.FirmwareVersion.Value() << std::endl;
      }

      std::cout << "Enabling ..." << std::endl;
      theAxis->enable();

      auto const startTime = std::chrono::steady_clock::now();

      int64_t pos = theAxis->position();

      const int delta = 800;

      unsigned int count = 0;
      while (!_done) {
        if ((count % 10) == 0) {
          std::chrono::duration<float> dt =
              (std::chrono::steady_clock::now() - startTime);
          std::cout << "[" << dt.count()
                    << "]  Pos = " << theAxis->position(false)
                    << " : Vel = " << theAxis->velocity()
                    << " : Torque = " << theAxis->torque() << std::endl;
        }

        if (kbhit() != 0) {
          auto ch = getchar();
          std::cout << "Got: " << ch << std::endl;
          if ((ch > 0) && (isprint(ch))) {
            if (toupper(ch) == '<') {
              pos -= delta;
              theAxis->positionMove(pos, true);
              std::cout << "NEW COMMANDED POSITION: " << theAxis->positionCmd()
                        << std::endl;
            } else if (toupper(ch) == '>') {
              pos += delta;
              theAxis->positionMove(pos, true);
              std::cout << "NEW COMMANDED POSITION: " << theAxis->positionCmd()
                        << std::endl;

            } else if (toupper(ch) == 'Q') {
              _done = true;
            }
          }
        }

        usleep(100000);
      }

      std::cout << " ... disabling" << std::endl;
      theAxis->disable();

    } catch (mnErr &theErr) {
      printf("Caught error: addr=%d, err=0x%08x\nmsg=%s\n", theErr.TheAddr,
             theErr.ErrorCode, theErr.ErrorMsg);
    }
  }

 private:
  Manager _mgr;

  //   CanInterface _canIf;
  //   int _canId;
  //   TMotor::MotorCommand _cmd;

  //   bool _initialized;

  bool _done;

  //   const float kp = 10.0;
  //   const float kd = 2.5;
};

int main(int argc, char **argv) {
  nonblock(true);

  int motorSn;

  try {
    // Parse command line options
    // clang-format off
    po::options_description options("Options");
    options.add_options()
        ("help", "produce help message")
        ("sn", po::value<int>()->default_value(-1));
    // clang-format on

    po::variables_map vm;
    po::store(parse_command_line(argc, argv, options), vm);
    po::notify(vm);

    if (vm.count("help")) {
      std::cout << options << std::endl;
      exit(0);
    }

    // Not sure how to motors are specified on the command line
    if (vm.count("sn") == 0) {
      ClearpathClient client;
      client.dumpNodes();
      exit(0);
    } else {
      motorSn = vm["sn"].as<int>();
    }

    ClearpathClient client;
    client.run(motorSn);

  } catch (const po::error &ex) {
    std::cerr << ex.what() << '\n';
    exit(-1);
  } catch (mnErr &theErr) {
    printf("Caught error: addr=%d, err=0x%08x\nmsg=%s\n", theErr.TheAddr,
           theErr.ErrorCode, theErr.ErrorMsg);

    exit(-1);
  }

  exit(0);
}
